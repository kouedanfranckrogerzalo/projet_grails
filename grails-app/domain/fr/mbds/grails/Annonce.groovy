package fr.mbds.grails

class Annonce {

    String title
    String description
    Float price
    Date dateCreated
    Date lastUpdated

    static hasMany = [illustrations: Illustration]

    static belongsTo = [author: User]

    static constraints = {
        title nullable: false, blank: false
        description nullable: false, blank: false, maxSize: 1000
        price nullable: false, min: 0F
        illustrations nullable: true, blank: false, maxSize: 500
    }

    static mapping = {
        description type: 'text'
        autoTimestamp true
    }
}
