
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body class="alert alert-primary" role="alert">
<div class="alert alert-primary" role="alert">
<center>

    <a href="#list-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

    <div class="nav" role="navigation">
        <div class="card-header">
            <div class="alert alert-danger" role="alert">
            <th>
        <tr>
            <td><a class="home btn btn-danger" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></td>
            <td><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></td>
        </tr>
            </th>
            </div>
        </div>
    </div>

    <div class="card">
    <div id="list-annonce" class="content scaffold-list" role="main"  >

        <div class="card-header">

        <h1><g:message code="default.list.label" args="[entityName]" /></h1>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                <tbody>
                <tr>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <td <f:table collection="${annonceList}" /> ></td>
        <div class="pagination">
           <td><g:paginate total="${annonceCount ?: 0}" /> </td>
        </div>
                </tr>
            </tbody>
            </thead>
            </table>
        </div>
    </div>
    </div>
</center>
</div>
</body>
</html>